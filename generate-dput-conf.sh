#!/bin/bash

set -e

# Generate a string that is the contents of a dput.cf file and send
# this string to standard output. This dput.cf is appropriate for
# Stanford's "stanford" and "local" Debian repositories.

# Set the allowed distributions string.
if [[ -z "$ALLOWED_DISTRIBUTIONS" ]]; then
    ALLOWED_DISTRIBUTIONS="unstable stable oldstable oldoldstable [a-z]+-backports"
    ALLOWED_DISTRIBUTIONS="$ALLOWED_DISTRIBUTIONS xenial focal"
fi

# Set the default SSH private key path
if [[ -z "$SSH_PRIVATE_KEY_PATH" ]]; then
    SSH_PRIVATE_KEY_PATH=/root/.ssh/id_rsa
fi

# Create an array from ALLOWED_DISTRIBUTIONS.
AD_ARRAY=($ALLOWED_DISTRIBUTIONS)

# Convert ALLOWED_DISTRIBUTIONS to a regex.
AD_REGEX=$(printf "|%s" "${AD_ARRAY[@]}")
AD_REGEX=${AD_REGEX:1}

cat dput.cf.template \
    | sed "s/%%ALLOWED_DISTRIBUTIONS_RX%%/$AD_REGEX/g"         \
    | sed "s@%%SSH_PRIVATE_KEY_PATH%%@$SSH_PRIVATE_KEY_PATH@g" \
    | sed "s@%%DPUT_USER%%@$DPUT_USER@g"
