#!/bin/bash

set -e

# Generate a GitLab CI/CD YAML file containing one job for each
# distribution in the list DEBIAN_DISTROS. Each of these jobs
# does a Debian package build against the Debian distribution.
#
# For example, if DEBIAN_DISTROS is "buster sid" then the output
# will be two jobs "build-debian-package-buster"
# and "build-debian-package-sid" which build a buster Debian package and
# a sid Debian package, respectively.
#
# This script should be used to generate a .gitlab-ci.yaml as an artifact
# that is the used by a child pipline via a trigger. See the file
# .gitlab-ci.yaml in this directory for an example.

# Define the current Debian distrubtion and codename mappings.
declare -A release_to_suite
release_to_suite['bullseye']='stable'
release_to_suite['buster']='oldstable'
release_to_suite['stretch']='oldoldstable'

declare -A release_to_number
release_to_number['stretch']='9'
release_to_number['buster']='10'
release_to_number['bullseye']='11'

if [[ -z "$DEBIAN_RELEASES" ]]; then
    DEBIAN_RELEASES="sid buster bullseye"
fi

cat header.yml

# Generate the "stages" section so that the builds
# run serially. This is not a technical requirement, rather
# it is being polite so we don't hog the build resources.
echo "stages:"
for RELEASE in $DEBIAN_RELEASES
do
    echo "  - $RELEASE"
done
echo

for RELEASE in $DEBIAN_RELEASES
do
    if [[ "$RELEASE" == "sid" ]]; then
        DCH_COMMAND="echo 'skipping dch since this is build for unstable (sid)'"
    else
        message="Stanford backport to $RELEASE."
        relnumber=${release_to_number[$RELEASE]}
        suite=${release_to_suite[$RELEASE]}
        localver="~sbp$relnumber+"
        DCH_COMMAND="dch --local=$localver --distribution=$suite '$message'"
    fi

    cat gitlab-ci.yml.template \
        | sed "s/%%DEBIAN_RELEASE%%/$RELEASE/g" \
        | sed "s/%%DCH_COMMAND%%/$DCH_COMMAND/g" \
        | sed "s/%%DPUT_HOST%%/$DPUT_HOST/g"
    echo
done
